$(function () {
    var APPLICATION_ID = "73203F30-918E-F331-FFF2-CAF516A56900",
        SECRET_KEY = "2168D6FB-DAF3-8DAA-FFC6-1750906E6000",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
      
     var loginScript = $("#login-template").html();
     var loginTemplate = Handlebars.compile(loginScript);
     
     
     $('.main-container').html(loginTemplate);
     
     $(document).on('submit', '.form-signin', function(event){
         event.preventDefault();
         
         var data = $(this).serializeArray(),
             email = data[0].value,
             password = data[1].vale;
             
        Backendless.UserService.login(email, password, true, new Backendless.Async(userLoggedIn, gotError));     
     });
});

function Posts(args) {
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail || "";
}
